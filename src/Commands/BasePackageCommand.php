<?php namespace NextLevels\BasePackageGenerator\Commands;

use NextLevels\BasePackageGenerator\Exceptions\GeneratorException;
use NextLevels\BasePackageGenerator\Generators\AbstractGenerator;
use NextLevels\BasePackageGenerator\Generators\BaseGenerator;
use NextLevels\BasePackageGenerator\Generators\DockerGenerator;
use NextLevels\BasePackageGenerator\Generators\HomesteadGenerator;
use NextLevels\BasePackageGenerator\Generators\ReadmeGenerator;
use NextLevels\BasePackageGenerator\Helper\GeneratorHelper;
use NextLevels\BasePackageGenerator\Helper\HomesteadHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class BasePackageCommand
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class BasePackageCommand extends Command
{

    /**
     * Configure the command options.
     */
    protected function configure(): void
    {
        $this
            ->setName('new')
            ->setDescription('Create a new base package project.')
            ->addArgument('name', InputArgument::REQUIRED, 'Project name');
    }

    /**
     * Execute the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws GeneratorException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $generatorHelper = GeneratorHelper::getInstance();
        $variables = $generatorHelper->getBaseVariables();
        $generatorHelper->setProjectName($generatorHelper->validateProjectName($input->getArgument('name')));
        $directory = sprintf('%s/%s', getcwd(), $generatorHelper->getProjectName());

        $io->title('Base Package Generator');

        if (file_exists($directory)) {
            $io->note(sprintf('The directory %s is already exists', $directory));

            if ($io->confirm('Want you delete the directory to continue?', false)) {
                try {
                    $generatorHelper->removeDirectoryRecursive($directory);
                } catch (\Exception $exception) {
                    $io->error(['Directory cannot delete.', 'Remove the directory and run the generator again']);

                    return Command::FAILURE;
                }
            } else {
                return Command::FAILURE;
            }
        }

        $distribution = $io->choice(
            'Please choose the project distribution',
            $generatorHelper->getAvailableDistributions(),
            'Blank'
        );

        $generatorHelper->setDistribution($distribution);

        $io->note(sprintf(
            'The project will take %s as distribution.',
            $generatorHelper->getDistribution()
        ));

        if (! $io->confirm('Is this correct?', false)) {
            return Command::FAILURE;
        }

        if (! file_exists($directory) && mkdir($directory) && is_dir($directory)) {
            $toDos = [[]];

            $generatorHelper->setGenerators([
                new BaseGenerator($generatorHelper->getProjectName(), $generatorHelper->getDistribution()),
                new HomesteadGenerator($generatorHelper->getProjectName()),
                new ReadmeGenerator($generatorHelper->getProjectName())
            ]);

            if ((
                $projectInstaller = $generatorHelper->getProjectInstaller(
                    $input,
                    $output,
                    sprintf('%s/web', $generatorHelper->getProjectName())
                )
            ) !== null
            ) {
                $projectInstaller->execute();
            }

            $generatorHelper->addGenerator(
                new DockerGenerator(
                    $generatorHelper->getProjectName(),
                    $generatorHelper->getDistribution(),
                    $variables
                )
            );

            /** @var AbstractGenerator $generator */
            foreach ($generatorHelper->getGenerators() as $generator) {
                $generator->run();
                $toDos[] = $generator->getToDos();
            }

            $toDos = array_merge(...$toDos);

            $io->title('Install environment packages');

            $process = Process::fromShellCommandline('composer install --no-suggest', $directory, null, null, null);
            $processSection = $output->section();

            $process->run(function ($type, $line) use ($processSection) {
                $processSection->overwrite($line);
            });

            if (! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            HomesteadHelper::updateHomesteadFile(
                $_SERVER['HOME'] . '/Homestead.yaml',
                $generatorHelper->getProjectName()
            );

            $processSection->overwrite('Environment installation finished');
            $io->newLine();

            if (! empty($toDos)) {
                $tableToDos = [];
                $table = new Table($output);

                foreach ($toDos as $toDo) {
                    $classSplit = explode('\\', $toDo['class']);
                    $tableToDos[] = [
                        str_replace('Generator', '', end($classSplit)),
                        $toDo['message']
                    ];
                }
                $table
                    ->setHeaders(['Generator', 'Todo'])
                    ->setRows($tableToDos)
                    ->setStyle('compact');

                $table->render();
            }

            $io->success('Everything is OK! Now lets work :-)');

            return Command::SUCCESS;
        }

        $io->error('Cannot create project directory');

        return Command::FAILURE;
    }
}

<?php namespace NextLevels\BasePackageGenerator\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class UpdateCommand
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class UpdateCommand extends Command
{

    /**
     * Configure the command options.
     */
    protected function configure(): void
    {
        $this
            ->setName('update')
            ->setDescription('Update base package generator.');
    }

    /**
     * Execute the command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $commandline = sprintf(
            '%s && %s',
            'composer global config discard-changes true',
            'composer global update nextlevels/base-package-generator --no-interaction'
        );

        $io->title('Update base package generator');

        $process = Process::fromShellCommandline($commandline);
        $processSection = $output->section();

        $process->run(function ($type, $line) use ($processSection) {
            $processSection->overwrite($line);
        });

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $processSection->overwrite('Update finished');
    }
}

<?php namespace NextLevels\BasePackageGenerator\Commands;

use NextLevels\BasePackageGenerator\Exceptions\GeneratorException;
use NextLevels\BasePackageGenerator\Helper\GeneratorHelper;
use NextLevels\BasePackageGenerator\Helper\HomesteadHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class HomesteadCommand
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class HomesteadCommand extends Command
{

    /**
     * Configure command
     */
    protected function configure(): void
    {
        $this
            ->setName('homestead')
            ->setDescription('Install Homestead and configure Homestead config files.')
            ->setHelp('This command allows you to update the global Homestead.yaml file.')
            ->addArgument('name', InputArgument::OPTIONAL, 'Project name')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Update all projects');
    }

    /**
     * Execute generate homestead command
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws GeneratorException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (isset($_SERVER['HOME']) && is_readable($_SERVER['HOME'] . '/Homestead.yaml')) {
            $io->title('Update global homestead.yaml file.');

            $generatorHelper = GeneratorHelper::getInstance();
            $updateProjects = [];

            if ($input->getArgument('name') !== null) {
                $updateProjects[] = $generatorHelper
                    ->setProjectName($generatorHelper->validateProjectName($input->getArgument('name')))
                    ->getProjectName();
            }

            if ($input->getOption('all') !== false) {
                $webDirectory = $_SERVER['HOME'] . '/web';

                if (is_dir($webDirectory)
                    && ($directories = scandir($webDirectory)) !== false
                ) {
                    foreach ($directories as $directory) {
                        if ($directory !== '.' && $directory !== '..' && is_dir($webDirectory . '/' . $directory)) {
                            $updateProjects[] = $directory;
                        }
                    }
                }
            }

            if (! empty($updateProjects)) {
                foreach ($updateProjects as $project) {
                    HomesteadHelper::updateHomesteadFile($_SERVER['HOME'] . '/Homestead.yaml', $project);
                }
            }

            $io->success('Homestead.yaml saved successfully');

            return Command::SUCCESS;
        }

        $io->error('Cannot find global Homestead.yaml file');

        return Command::FAILURE;
    }
}

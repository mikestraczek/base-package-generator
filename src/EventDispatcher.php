<?php namespace NextLevels\BasePackageGenerator;

use Composer\Script\Event;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class EventDispatcher
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class EventDispatcher
{

    /**
     * Dispatches the install command
     *
     * @param Event $event
     */
    public static function postInstallCommand(Event $event): void
    {
        if (file_exists(getcwd() . '/web/composer.json')) {
            $io = $event->getIO();
            $process = new Process('composer i --no-progress --no-interaction', getcwd() . '/web', null, null, null);

            $process->run(function ($type, $line) use ($io) {
                $io->write($line);
            });

            if (! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
        }
    }

    /**
     * Dispatches the update command
     *
     * @param Event $event
     */
    public static function postUpdateCommand(Event $event): void
    {
        if (file_exists(getcwd() . '/web/composer.json')) {
            $io = $event->getIO();
            $process = new Process('composer u --no-progress --no-interaction', getcwd() . '/web', null, null, null);

            $process->run(function ($type, $line) use ($io) {
                $io->write($line);
            });

            if (! $process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
        }
    }
}

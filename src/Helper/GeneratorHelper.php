<?php namespace NextLevels\BasePackageGenerator\Helper;

use NextLevels\BasePackageGenerator\Exceptions\GeneratorException;
use NextLevels\BasePackageGenerator\Generators\BaseThemeGenerator;
use NextLevels\BasePackageGenerator\Generators\LaravelEnvGenerator;
use NextLevels\BasePackageGenerator\Generators\LaravelMixGenerator;
use NextLevels\BasePackageGenerator\Generators\OctoberCMSGenerator;
use NextLevels\BasePackageGenerator\Generators\ShopwareGenerator;
use NextLevels\BasePackageGenerator\Generators\TYPO3Generator;
use NextLevels\BasePackageGenerator\Generators\WordpressGenerator;
use NextLevels\Installer\ContaoInstaller;
use NextLevels\Installer\LaravelInstaller;
use NextLevels\Installer\OctoberCMSInstaller;
use NextLevels\Installer\ShopwareInstaller;
use NextLevels\Installer\SymfonyInstaller;
use NextLevels\Installer\TYPO3Installer;
use NextLevels\Installer\WordpressInstaller;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GeneratorHelper
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class GeneratorHelper
{

    /**
     * @var string
     */
    protected $webServer = 'apache2';

    /**
     * @var
     */
    protected $projectName;

    /**
     * @var string
     */
    protected $distribution;

    /**
     * @var array
     */
    protected $availableDistributions;

    /**
     * @var array
     */
    protected $baseVariables;

    /**
     * @var
     */
    protected $generators;

    /**
     * @var GeneratorHelper
     */
    private static $instance;

    /**
     * GeneratorHelper constructor.
     */
    public function __construct()
    {
        $this->setAvailableDistributions([
            'Blank',
            'Laravel',
            'OctoberCMS',
            'Wordpress',
            'Contao (Beta)',
            'Shopware',
            'TYPO3',
            'Symfony'
        ]);

        $this->setBaseVariables([
            'rootPath' => '',
            'mysqli'   => 'false',
            'zip'      => 'true',
            'shopware' => false
        ]);
    }

    /**
     * Return the instance of GeneratorHelper
     *
     * @return GeneratorHelper
     */
    public static function getInstance(): self
    {
        if (! self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return string
     */
    public function getWebServer(): string
    {
        return $this->webServer;
    }

    /**
     * @param string $webServer
     *
     * @return GeneratorHelper
     */
    public function setWebServer(string $webServer): self
    {
        $this->webServer = $webServer;

        return $this;
    }

    /**
     * @return array
     */
    public function getAvailableDistributions(): array
    {
        return $this->availableDistributions;
    }

    /**
     * @param array $availableDistributions
     *
     * @return GeneratorHelper
     */
    public function setAvailableDistributions($availableDistributions): self
    {
        $this->availableDistributions = $availableDistributions;

        return $this;
    }

    /**
     * @return array
     */
    public function getBaseVariables(): array
    {
        return $this->baseVariables;
    }

    /**
     * @param array $variables
     *
     * @return GeneratorHelper
     */
    public function setBaseVariables($variables): self
    {
        $this->baseVariables = $variables;

        return $this;
    }

    /**
     * @return array
     */
    public function getGenerators(): array
    {
        return $this->generators;
    }

    /**
     * @param array $generators
     *
     * @return GeneratorHelper
     */
    public function setGenerators($generators): self
    {
        $this->generators = $generators;

        return $this;
    }

    /**
     * @param array|object $generator
     *
     * @return GeneratorHelper
     */
    public function addGenerator($generator): self
    {
        if (\is_array($generator)) {
            $this->generators = array_merge($this->generators, $generator);

            return $this;
        }

        $this->generators[] = $generator;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param mixed $projectName
     *
     * @return GeneratorHelper
     */
    public function setProjectName($projectName): self
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDistribution(): string
    {
        return $this->distribution;
    }

    /**
     * @param string $distribution
     *
     * @return GeneratorHelper
     */
    public function setDistribution(string $distribution): self
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Validate given project name
     *
     * @param string $projectName
     *
     * @return null|string|string[]
     * @throws GeneratorException
     */
    public function validateProjectName($projectName)
    {
        $invalidCharCount = 0;
        $projectName = preg_replace('/\W/', '_', trim($projectName), -1, $invalidCharCount);

        if ($invalidCharCount > 0) {
            throw new GeneratorException(
                'Project name should only contain letters (a-z, A-Z), numbers (0-9) and underscores.'
            );
        }

        return $projectName;
    }

    /**
     * Remove folders and files
     *
     * @param $dir
     */
    public function removeDirectoryRecursive($dir): void
    {
        if (is_dir($dir)) {
            $files = scandir($dir, SCANDIR_SORT_NONE);

            foreach ($files as $file) {
                if ($file !== '.' && $file !== '..') {
                    $this->removeDirectoryRecursive($dir . '/' . $file);
                }
            }

            @rmdir($dir);
        } else {
            @unlink($dir);
        }
    }

    /**
     * Get project installer bei given project name and project distribution
     *
     * @param InputInterface|null  $input
     * @param OutputInterface|null $output
     * @param string               $webDir
     *
     * @return ContaoInstaller|LaravelInstaller|OctoberCMSInstaller|ShopwareInstaller|SymfonyInstaller|TYPO3Installer|WordpressInstaller|null
     */
    public function getProjectInstaller(
        InputInterface $input = null,
        OutputInterface $output = null,
        string $webDir = 'web'
    ) {
        $distribution = $this->getDistribution();
        $projectInstaller = null;
        $projectName = $this->getProjectName();

        switch ($distribution) {
            case 'Laravel':
                $variables['rootPath'] = '/public';

                $this->addGenerator(new LaravelEnvGenerator($projectName));

                $projectInstaller = new LaravelInstaller($input, $output, $webDir);
                break;
            case 'OctoberCMS':
                $variables['zip'] = 'true';

                $this->addGenerator([
                    new OctoberCMSGenerator($projectName),
                    new BaseThemeGenerator($projectName),
                    new LaravelMixGenerator($projectName),
                ]);

                $projectInstaller = new OctoberCMSInstaller($input, $output, $webDir, []);
                break;
            case 'Wordpress':
                $variables['mysqli'] = 'true';

                $this->addGenerator(new WordpressGenerator($projectName));

                $projectInstaller = new WordpressInstaller($input, $output, $webDir);
                break;
            case 'Contao (Beta)':
                $variables['rootPath'] = '/web';

                $projectInstaller = new ContaoInstaller($input, $output, $webDir);
                break;
            case 'Shopware':
                $variables['shopware'] = true;

                $this->addGenerator(new ShopwareGenerator($projectName));

                $projectInstaller = new ShopwareInstaller($input, $output, $webDir);
                break;
            case 'TYPO3':
                $variables['mysqli'] = 'true';
                $variables['zip'] = 'true';

                $this->addGenerator(new TYPO3Generator($projectName));

                $projectInstaller = new TYPO3Installer($input, $output, $webDir, [
                    'name'         => $projectName,
                    'distribution' => $distribution
                ]);
                break;
            case 'Symfony':
                $variables['rootPath'] = '/public';

                $projectInstaller = new SymfonyInstaller($input, $output, $webDir);
                break;
        }

        return $projectInstaller;
    }
}

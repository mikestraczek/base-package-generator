<?php namespace NextLevels\BasePackageGenerator\Helper;

use Symfony\Component\Yaml\Yaml;

/**
 * Class HomesteadHelper
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>
 */
class HomesteadHelper
{

    /**
     * Update global homestead file
     *
     * @param string $file
     * @param string $projectName
     */
    public static function updateHomesteadFile(string $file, string $projectName): void
    {
        if (is_dir($_SERVER['HOME'] . '/web/' . $projectName . '/web')) {
            $homesteadData = Yaml::parseFile($file);

            if (isset($homesteadData['sites'])) {
                $map = str_replace('_', '-', $projectName) . '.local';
                $to = '/home/vagrant/web/' . $projectName . '/web';

                if (($key = array_search($map, array_column($homesteadData['sites'], 'map'), false)) !== false) {
                    $homesteadData['sites'][$key] = [
                        'map'  => $map,
                        'to'   => $to,
                        'type' => 'apache'
                    ];
                } else {
                    $homesteadData['sites'][] = [
                        'map'  => $map,
                        'to'   => $to,
                        'type' => 'apache'
                    ];
                }
            }

            if (isset($homesteadData['databases'])) {
                if (($key = array_search($projectName, $homesteadData['databases'], false)) !== false) {
                    $homesteadData['databases'][$key] = $projectName;
                } else {
                    $homesteadData['databases'][] = $projectName;
                }
            }

            file_put_contents($file, Yaml::dump($homesteadData));
        }
    }
}

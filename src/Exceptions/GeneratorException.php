<?php namespace NextLevels\BasePackageGenerator\Exceptions;

/**
 * Class GeneratorException
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class GeneratorException extends \Exception
{

    // Empty
}

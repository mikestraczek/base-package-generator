<?php namespace NextLevels\BasePackageGenerator\Exceptions;

/**
 * Class DispatcherException
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class DispatcherException extends \Exception
{

    // Empty
}

<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class HomesteadGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class HomesteadGenerator extends AbstractGenerator
{

    /**
     * Run generator.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->setFiles([
            'Homestead.yaml' => '',
            'Vagrantfile'    => ''
        ]);

        foreach ($this->getFiles() as $file => $dist) {
            $this->dumpFile(
                $file,
                $this->getBaseDirectory(),
                $this->variables
            );
        }
    }
}

<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class BaseThemeGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class BaseThemeGenerator extends AbstractGenerator
{

    /**
     * Run generator.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $themeFolder = sprintf('web/themes/%s/', $this->getProjectName());
        $themeDirectories = [
            'assets/' => [
                'folders' => ['css/', 'fonts/', 'images/', 'js/src', 'js/dist', 'js/final', 'scss/modules/'],
                'files'   => [
                    'css/app.css',
                    'js/src/app.js',
                    'js/dist/app.js',
                    'scss/app.scss',
                    'scss/modules/_mixins.scss',
                    'scss/modules/_utilities.scss',
                    'scss/modules/_variables.scss'
                ]
            ]
        ];

        $this->createDirectorySkeleton($themeFolder);
        $this->createRecursiveDataStructure($themeFolder, $themeDirectories);

        $this->createDirectorySkeleton($themeFolder . 'layouts/');
        $this->createDirectorySkeleton($themeFolder . 'pages/');
        $this->createDirectorySkeleton($themeFolder . 'partials/');

        $this->dumpFileWithOutTwig('default.htm', $this->getBaseDirectory() . $themeFolder . 'layouts/');
        $this->dumpFileWithOutTwig('index.htm', $this->getBaseDirectory() . $themeFolder . 'pages/');
        $this->dumpFileWithOutTwig('navigation.htm', $this->getBaseDirectory() . $themeFolder . 'partials/');
        $this->dumpFileWithOutTwig(
            'images/hero_example_image.jpeg',
            $this->getBaseDirectory() . $themeFolder . 'assets/'
        );
        $this->dumpFile('theme.yaml', $this->getBaseDirectory() . $themeFolder, $this->variables);
    }
}

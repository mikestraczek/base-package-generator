<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class LaravelEnvGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class LaravelEnvGenerator extends AbstractGenerator
{

    /**
     * Runs the laravel env generator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->dumpFile('.env', $this->getBaseDirectory() . 'web/', $this->variables);
    }
}

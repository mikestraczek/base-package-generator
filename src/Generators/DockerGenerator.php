<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class DockerGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class DockerGenerator extends AbstractGenerator
{

    /**
     * Runs OctoberCMS generator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->setFiles(['docker-compose.yml', 'docker-compose.sync.yml', 'sync.sh', 'docker-sync.yml']);

        foreach ($this->getFiles() as $file) {
            $this->dumpFile(
                $file,
                $this->getBaseDirectory(),
                $this->variables
            );
        }
    }
}

<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ReadmeGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class ReadmeGenerator extends AbstractGenerator
{

    /**
     * Runs the base readme creator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->dumpFile('README.md', $this->getBaseDirectory(), $this->variables);

        $this->addToDo('Please update the README.md.');
    }
}

<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ShopwareGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class ShopwareGenerator extends AbstractGenerator
{

    /**
     * Runs the base Shopware generator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->dumpFile('config.php', $this->getBaseDirectory() . 'web/', $this->variables);
        $this->dumpFile('.env', $this->getBaseDirectory() . 'web/', $this->variables);

        $this->addToDo(
            'Connect to docker box with "docker-compose exec php-fpm sh" and execute following script
            "app/install.sh"'
        );
    }
}

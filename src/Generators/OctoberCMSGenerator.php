<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class OctoberCMSGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class OctoberCMSGenerator extends AbstractGenerator
{

    /**
     * Run the OctoberCMS generator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->setVariables(array_merge($this->getVariables(), [
            'formattedProjectName' => str_replace(' ', '', ucwords(str_replace('_', ' ', $this->getProjectName())))
        ]));

        $pluginsFolder = sprintf('web/plugins/nextlevels/%s', str_replace('_', '', $this->getProjectName()));
        $pluginsDirectories = [
            '/'             => [
                'folders' => ['/lang/de/', '/models'],
                'files'   => [
                    '.gitignore',
                    'Plugin.php',
                    'plugin.yaml',
                    'composer.json',
                    'package.json',
                    'webpack.mix.js',
                    'lang/de/lang.php'
                ]
            ],
            '/components/'  => [
                'files' => [
                    '.gitkeep'
                ]
            ],
            '/controllers/' => [
                'files' => [
                    '.gitkeep'
                ]
            ],
            '/models/'      => [
                'files' => [
                    '.gitkeep'
                ]
            ],
            '/updates/'     => [
                'files' => [
                    'version.yaml'
                ]
            ]
        ];

        $this->createDirectorySkeleton($pluginsFolder);
        $this->createRecursiveDataStructure($pluginsFolder, $pluginsDirectories);

        $this->dumpFile('.env', $this->getBaseDirectory() . 'web/', $this->variables);

        $this->addToDo(
            'Connect to docker with "docker-compose exec php-fpm sh" or to vagrantbox with "vagrant ssh" and
            execute follow command to start the install wizard: "php artisan october:install"'
        );
        $this->addToDo(sprintf(
            'Also execute follow command to enable base theme "php artisan theme:use %s"',
            $this->getProjectName()
        ));
    }
}

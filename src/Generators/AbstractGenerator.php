<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * Class AbstractGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
abstract class AbstractGenerator
{

    /**
     * @var string
     */
    protected $baseDirectory;

    /**
     * Saves the project name
     *
     * @var string
     */
    protected $projectName;

    /**
     * Saves the project name
     *
     * @var string
     */
    protected $projectDistribution;

    /**
     * Installer variables
     *
     * @var array
     */
    protected $variables;

    /**
     * Saves the twig instance
     *
     * @var Environment
     */
    protected $twig;

    /**
     * Saves the toDos
     *
     * @var array
     */
    protected $toDos = [];

    /**
     * @var array
     */
    protected $files = [];

    /**
     * AbstractGenerator constructor.
     *
     * @param string      $projectName
     * @param string      $projectDistribution
     * @param array       $variables
     * @param string|null $customDirectory
     */
    public function __construct
    (string $projectName,
        string $projectDistribution = '',
        array $variables = [],
        string $customDirectory = null
    )
    {
        $this->projectName = $projectName;
        $this->projectDistribution = $projectDistribution;
        $this->variables = $variables;

        if ($customDirectory !== null) {
            $this->baseDirectory = $customDirectory;
        } else {
            $this->baseDirectory = sprintf('%s/%s/', getcwd(), $projectName);
        }
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     *
     * @return AbstractGenerator
     */
    public function setVariables(array $variables = []): self
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * @param array $files
     *
     * @return AbstractGenerator
     */
    public function setFiles(array $files): self
    {
        $this->files = $files;

        return $this;
    }

    /**
     * @param string $file
     *
     * @return AbstractGenerator
     */
    public function addFile($file = ''): self
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Adds a TODO message
     *
     * @param string $message
     */
    protected function addToDo(string $message): void
    {
        $this->toDos[] = ['class' => static::class, 'message' => $message];
    }

    /**
     * Return the toDos
     *
     * @return array
     */
    public function getToDos(): array
    {
        return $this->toDos;
    }

    /**
     * Return the project name
     *
     * @return string
     */
    public function getProjectName(): string
    {
        return $this->projectName;
    }

    /**
     * Return the project namespace
     *
     * @return string
     */
    protected function getProjectNamespace(): string
    {
        return str_replace(
            ' ',
            '',
            ucwords(
                str_replace('_', ' ', strtolower($this->projectName))
            )
        );
    }

    /**
     * Returns the project hostname
     *
     * @return string
     */
    protected function getProjectHostname(): string
    {
        return str_replace(
            '_',
            '-',
            $this->projectName
        );
    }

    /**
     * Return the base directory
     *
     * @return string
     */
    public function getBaseDirectory(): string
    {
        return $this->baseDirectory;
    }

    /**
     * @param string $baseDirectory
     *
     * @return AbstractGenerator
     */
    public function setBaseDirectory(string $baseDirectory): self
    {
        $this->baseDirectory = $baseDirectory;

        return $this;
    }

    /**
     * Returns the skeleton base path
     *
     * @return string
     */
    private function getSkeletonBaseDirectory(): string
    {
        $splitClass = explode('\\', static::class);
        $generatorClass = end($splitClass);

        return realpath(__DIR__ . '/' . $generatorClass . '/skeleton') . DIRECTORY_SEPARATOR;
    }

    /**
     * Return the twig instance
     *
     * @return Environment
     */
    protected function getTwig(): Environment
    {
        if (! $this->twig) {
            $loader = new FilesystemLoader($this->getSkeletonBaseDirectory());
            $this->twig = new Environment($loader);
        }

        return $this->twig;
    }

    /**
     * Creates the directories if not existing
     *
     * @param string $directory
     *
     * @return bool|string
     */
    protected function createDirectorySkeleton($directory)
    {
        $directory = str_replace('/', DIRECTORY_SEPARATOR, $this->getBaseDirectory() . $directory);

        if (! file_exists($directory) && mkdir($directory, 0755, true) && is_dir($directory)) {
            return is_dir($directory) ? realpath($directory) . DIRECTORY_SEPARATOR : false;
        }

        return false;
    }

    /**
     * Returns the base variables
     *
     * @return array
     */
    private function getBaseVariables(): array
    {
        return [
            'projectName'         => $this->getProjectName(),
            'projectDistribution' => $this->projectDistribution,
            'projectNamespace'    => $this->getProjectNamespace(),
            'projectHostname'     => $this->getProjectHostname(),
            'timestamp'           => time(),
        ];
    }

    /**
     * @param string $path
     * @param string $writePath
     * @param array  $variables
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function dumpFile(string $path, string $writePath, array $variables = []): void
    {
        $filepath = $this->getProjectName() . '/' . $path;
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);

        $variables = array_merge(
            $variables,
            $this->getBaseVariables(),
            [
                'filename' => $path,
                'filepath' => $filepath,
            ]
        );

        file_put_contents(
            $writePath . $path,
            $this->getTwig()->render($path . '.twig', $variables)
        );
    }

    /**
     * @param string $path
     * @param string $writePath
     */
    protected function dumpFileWithOutTwig(string $path, string $writePath): void
    {
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);

        copy($this->getSkeletonBaseDirectory() . $path, $writePath . $path);
    }

    /**
     * Create given data structure
     *
     * @param string $folder
     * @param array  $dataStructure
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function createRecursiveDataStructure(string $folder, array $dataStructure): void
    {
        foreach ($dataStructure as $directory => $values) {
            $this->createDirectorySkeleton($folder . $directory);

            if (\is_array($values)) {
                foreach ($values as $type => $subDirectories) {
                    switch ($type) {
                        case 'folders':
                            if (\is_array($subDirectories)) {
                                foreach ($subDirectories as $subDirectory) {
                                    $this->createDirectorySkeleton(
                                        $folder . $directory . $subDirectory
                                    );
                                }
                            }
                            break;

                        case 'files':
                            if (\is_array($subDirectories)) {
                                foreach ($subDirectories as $file) {
                                    $this->dumpFile(
                                        $file,
                                        $this->getBaseDirectory() . $folder . $directory,
                                        $this->variables
                                    );
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    /**
     * Runs the generator
     */
    abstract public function run();
}

<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class WordpressGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class WordpressGenerator extends AbstractGenerator
{

    /**
     * Runs the wordpress generator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->dumpFile('wp-config.php', $this->getBaseDirectory() . 'web/', $this->variables);
    }
}

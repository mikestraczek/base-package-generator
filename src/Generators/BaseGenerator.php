<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class BaseGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class BaseGenerator extends AbstractGenerator
{

    /**
     * Run generator.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->createDirectorySkeleton('web');
        $this->setFiles([
            '.gitignore'    => '',
            '.php_cs'       => '',
            'composer.json' => '',
            'deploy.php'    => ''
        ]);

        foreach ($this->getFiles() as $file => $dist) {
            $this->dumpFile(
                $file,
                $this->getBaseDirectory(),
                $this->variables
            );
        }

        $this->addToDo(
            'Change server credentials and set project repository url in deploy.php file before you use deployer...'
        );
    }
}

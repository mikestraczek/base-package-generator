<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class TYPO3Generator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class TYPO3Generator extends AbstractGenerator
{

    /**
     * Runs the typo3 generator
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->dumpFile('FIRST_INSTALL', $this->getBaseDirectory() . 'web/', $this->variables);
        $this->dumpFile('.gitignore', $this->getBaseDirectory() . 'web/', $this->variables);
    }
}

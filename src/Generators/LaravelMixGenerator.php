<?php namespace NextLevels\BasePackageGenerator\Generators;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class LaravelMixGenerator
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class LaravelMixGenerator extends AbstractGenerator
{

    /**
     * Run laravel mix generator.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function run(): void
    {
        $this->setFiles([
            'webpack.mix.js' => '',
            'package.json'   => ''
        ]);

        foreach ($this->getFiles() as $file => $dist) {
            $this->dumpFile(
                $file,
                $this->getBaseDirectory() . $dist,
                $this->variables
            );
        }
    }
}

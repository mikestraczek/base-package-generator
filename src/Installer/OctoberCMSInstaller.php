<?php namespace NextLevels\Installer;

use RuntimeException;

/**
 * Class OctoberCMSInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class OctoberCMSInstaller extends AbstractInstaller
{

    /**
     * Execute the command.
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building OctoberCMS project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $composer = $this->findComposer();
            $repoUrl = 'https://Satis:Satis1337!@satis.next-levels.de';
            $commands = [
                $composer . ' create-project october/october:1.1.* . --no-dev',
                $composer . " config repositories.satis composer $repoUrl",
                $composer . ' require nextlevels/essentials'
            ];

            if ($io->confirm('Do you want install all important rainlab plugins for OctoberCMS?', false)) {
                $commands = array_merge($commands, [
                    $composer . ' require rainlab/user-plugin',
                    $composer . ' require rainlab/builder-plugin',
                    $composer . ' require rainlab/pages-plugin',
                    $composer . ' require rainlab/blog-plugin',
                    $composer . ' require rainlab/translate-plugin'
                ]);
            }

            $this->runCommandLine(implode(' && ', $commands), $directory);
        }
    }
}

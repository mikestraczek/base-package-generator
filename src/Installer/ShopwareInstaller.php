<?php namespace NextLevels\Installer;

use RuntimeException;

/**
 * Class ShopwareInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class ShopwareInstaller extends AbstractInstaller
{

    /**
     * Execute the command.
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        if (! \function_exists('gd_info')) {
            throw new RuntimeException('The gd2 PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building Shopware project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $this->download(
                $zipFile = $this->makeFilename('shopware'),
                'https://www.shopware.com/de/Download/redirect/version/sw6/file/install_6.1.4_1584369563.zip'
            )
                ->extract($zipFile, $directory)
                ->cleanUp($zipFile);

            $composer = $this->findComposer();

            $this->runCommandLine($composer . ' install --no-scripts', $directory);
        }
    }
}

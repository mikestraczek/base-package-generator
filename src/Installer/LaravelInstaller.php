<?php namespace NextLevels\Installer;

use RuntimeException;

/**
 * Class LaravelInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class LaravelInstaller extends AbstractInstaller
{

    /**
     * Execute the command.
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building laravel project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $this->download($zipFile = $this->makeFilename('laravel'), 'http://cabinet.laravel.com/latest.zip')
                ->extract($zipFile, $directory)
                ->cleanUp($zipFile);

            $composer = $this->findComposer();
            $commands = [
                $composer . ' install --no-scripts',
                $composer . ' run-script post-root-package-install',
                $composer . ' run-script post-install-cmd',
                $composer . ' run-script post-create-project-cmd',
            ];

            $this->runCommandLine(implode(' && ', $commands), $directory);
        }
    }
}

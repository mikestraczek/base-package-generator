<?php namespace NextLevels\Installer;

use NextLevels\BasePackageGenerator\Helper\GeneratorHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Class AbstractInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
abstract class AbstractInstaller
{

    /**
     * Name of target directory
     *
     * @var string
     */
    protected $name;

    /**
     * Input interface
     *
     * @var InputInterface|null
     */
    protected $input;

    /**
     * Output interface
     *
     * @var OutputInterface|null
     */
    protected $output;

    /**
     * Project data
     *
     * @var array
     */
    protected $projectData;

    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * AbstractInstaller constructor.
     *
     * @param InputInterface|null  $input
     * @param OutputInterface|null $output
     * @param string               $name
     * @param array                $projectData
     */
    public function __construct(
        InputInterface $input = null,
        OutputInterface $output = null,
        string $name = 'web',
        array $projectData = []
    ) {
        $this
            ->setInput($input)
            ->setOutput($output)
            ->setName($name)
            ->setProjectData($projectData)
            ->setIo($input, $output);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return AbstractInstaller
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return InputInterface
     */
    public function getInput(): InputInterface
    {
        return $this->input;
    }

    /**
     * @param InputInterface|null $input
     *
     * @return AbstractInstaller
     */
    public function setInput(InputInterface $input = null): self
    {
        $this->input = $input;

        return $this;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    /**
     * @param OutputInterface|null $output
     *
     * @return AbstractInstaller
     */
    public function setOutput(OutputInterface $output = null): self
    {
        $this->output = $output;

        return $this;
    }

    /**
     * @return array
     */
    public function getProjectData(): array
    {
        return $this->projectData;
    }

    /**
     * @param array $projectData
     *
     * @return AbstractInstaller
     */
    public function setProjectData(array $projectData): self
    {
        $this->projectData = $projectData;

        return $this;
    }

    /**
     * @return SymfonyStyle
     */
    public function getIo(): SymfonyStyle
    {
        return $this->io;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return AbstractInstaller
     */
    public function setIo(InputInterface $input, OutputInterface $output): self
    {
        $this->io = new SymfonyStyle($input, $output);

        return $this;
    }

    /**
     * Verify that the application does not already exist.
     *
     * @param string $directory
     */
    protected function verifyApplicationDoesntExist(string $directory): void
    {
        if ($directory !== getcwd() && (is_dir($directory) || is_file($directory))) {
            throw new \RuntimeException('Application already exists!');
        }
    }

    /**
     * Extract the Zip file into the given directory.
     *
     * @param string $zipFile
     * @param string $directory
     *
     * @return $this
     */
    protected function extract(string $zipFile, string $directory): self
    {
        $archive = new \ZipArchive();

        if ($archive->open($zipFile)) {
            $archive->extractTo($directory);

            $archive->close();
        } else {
            throw new \RuntimeException('Cant open this file' . $zipFile . '!');
        }

        return $this;
    }

    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer(): string
    {
        if (file_exists(getcwd() . '/composer.phar')) {
            return '"' . PHP_BINARY . '" composer.phar';
        }

        return 'composer';
    }

    /**
     * Generate a random temporary filename.
     *
     * @param string $name
     *
     * @return string
     */
    protected function makeFilename(string $name): string
    {
        return sprintf(
            '%s/%s_%s.zip',
            getcwd(),
            $name,
            md5(time() . uniqid('', true))
        );
    }

    /**
     * Download the temporary Zip to the given file.
     *
     * @param string $zipFile
     * @param string $url
     *
     * @return $this
     */
    protected function download(string $zipFile, string $url): self
    {
        file_put_contents($zipFile, fopen($url, 'rb'));

        return $this;
    }

    /**
     * Clean-up the Zip file.
     *
     * @param string $zipFile
     *
     * @return $this
     */
    protected function cleanUp(string $zipFile): self
    {
        @chmod($zipFile, 0777);

        @unlink($zipFile);

        return $this;
    }

    /**
     * Remove folders and files
     *
     * @param string $dir
     */
    protected function removeDirectoryRecursive(string $dir): void
    {
        GeneratorHelper::getInstance()->removeDirectoryRecursive($dir);
    }

    /**
     * Copy folders and files recursive
     *
     * @param string $source
     * @param string $destination
     */
    protected function copyRecursive(string $source, string $destination): void
    {
        if (is_dir($source)) {
            if (! file_exists($destination) && mkdir($destination, 0755, true) && is_dir($destination)) {
                $files = scandir($source, SCANDIR_SORT_NONE);

                foreach ($files as $file) {
                    if ($file !== '.' && $file !== '..') {
                        $this->copyRecursive($source . '/' . $file, $destination . '/' . $file);
                    }
                }
            }
        } elseif (file_exists($source) && copy($source, $destination)) {
            $this->removeDirectoryRecursive($source);
        }
    }

    /**
     * Run given command line in given directory
     *
     * @param string $commandLine
     * @param string $directory
     */
    protected function runCommandLine(string $commandLine, string $directory): void
    {
        $process = Process::fromShellCommandline($commandLine, $directory, null, null, null);
        $processSection = $this->getOutput()->section();

        $process->run(function ($type, $line) use ($processSection) {
            $processSection->overwrite($line);
        });

        if (! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $processSection->overwrite('Installation finished');
        $this->getIo()->newLine();
    }

    abstract public function execute();
}

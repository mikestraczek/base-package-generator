<?php namespace NextLevels\Installer;

use RuntimeException;

/**
 * Class SymfonyInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class SymfonyInstaller extends AbstractInstaller
{

    /**
     * Execute the command.
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building Symfony project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $composer = $this->findComposer();

            $this->runCommandLine($composer . ' create-project symfony/skeleton .', $directory);
        }
    }
}

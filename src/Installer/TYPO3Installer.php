<?php namespace NextLevels\Installer;

use RuntimeException;

/**
 * Class TYPO3Installer
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class TYPO3Installer extends AbstractInstaller
{

    /**
     * Execute the command.
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building TYPO3 project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $composer = $this->findComposer();
            $commands = [
                $composer . sprintf(
                    ' init --name=nextlevels/%s --author="Next Levels <info@next-levels.de>" -l=MIT',
                    $this->projectData['name']
                ),
                $composer . ' require typo3/cms',
                $composer . ' require bk2k/bootstrap-package'
            ];

            $this->runCommandLine(implode(' && ', $commands), $directory);
        }
    }
}

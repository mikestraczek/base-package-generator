<?php namespace NextLevels\Installer;

use RuntimeException;

/**
 * Class ContaoInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class ContaoInstaller extends AbstractInstaller
{

    /**
     * Execute the command.
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        if (! \function_exists('gd_info')) {
            throw new RuntimeException('The gd2 PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building contao project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $composer = $this->findComposer();
            $command = $composer . ' create-project contao/managed-edition .';

            $this->runCommandLine($command, $directory);
        }
    }
}

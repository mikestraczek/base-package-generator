<?php namespace NextLevels\Installer;

/**
 * Class WordpressInstaller
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class WordpressInstaller extends AbstractInstaller
{

    /**
     * Move wordpress folder content to web folder and remove wordpress folder
     *
     * @param string $directory
     *
     * @return $this
     */
    protected function moveContentToWebFolder(string $directory): self
    {
        $this->copyRecursive($directory . '/wordpress', $directory);
        $this->removeDirectoryRecursive($directory . '/wordpress');

        return $this;
    }

    /**
     * Execute wordpress installer
     */
    public function execute(): void
    {
        if (! class_exists('ZipArchive')) {
            throw new \RuntimeException('The Zip PHP extension is not installed. Please install it and try again.');
        }

        $io = $this->getIo();
        $zipFile = $this->makeFilename('wordpress');
        $directory = ! empty($this->name) ? getcwd() . '/' . $this->name : getcwd();

        $this->verifyApplicationDoesntExist($directory);
        $io->title('Building wordpress project...');

        if (mkdir($directory, 0755, true) && is_dir($directory)) {
            $this
                ->download($zipFile, 'https://de.wordpress.org/latest-de_DE.zip')
                ->extract($zipFile, $directory)
                ->cleanUp($zipFile)
                ->moveContentToWebFolder($directory);
        }
    }
}

# Base Package Generator

## How to use
* First install the base package generator with following command
    * `composer global config repositories.satis composer 'http://Satis:Satis1337!@satis.next-levels.de' && composer global config secure-http false`
    * `composer global require nextlevels/base-package-generator:dev-global-installer`
* Run in terminal `base-package new project-name`

## ToDos

* Fix shopware missing test folder in wizard installer
* Fix TYPO3 Installer if user want install TYPO3 9.3
    
    
## Issues

* If you get "-bash: base-package: command not found" error, then use following command to register all composer bin files
    * `export PATH=~/.composer/vendor/bin:$PATH`
<?php namespace Deployer;

require 'recipe/common.php';
require 'octobercms.php';

use Deployer\Exception\ConfigurationException;

/**
 * Override symlink task
 */
desc('Creating symlink to release');
task('deploy:symlink', function () {
    set('new_release_path', str_replace(get('deploy_path') . '/', '', get('release_path')));

    if (run('if [[ "$(man mv)" =~ "--no-target-directory" ]]; then echo "true"; fi')->toBool()) {
        run('mv -T {{deploy_path}}/release {{deploy_path}}/current');
    } else {
        run('cd {{deploy_path}} && {{bin/symlink}} {{new_release_path}}/web current');
        run('cd {{deploy_path}} && rm release');
    }
});

/**
 * Override vendor task
 */
desc('Installing vendors');
task('deploy:vendors', function () {
    run('cd {{release_path}}/web && composer {{composer_options}}');
});

/**
 * Override shared task
 */
desc('Creating symlinks for shared files and dirs');
task('deploy:shared', function () {
    $sharedPath = '{{deploy_path}}/shared';

    // Validate shared_dir, find duplicates
    foreach (get('shared_dirs') as $a) {
        foreach (get('shared_dirs') as $b) {
            if ($a !== $b && strpos(rtrim($a, '/') . '/', rtrim($b, '/') . '/') === 0) {
                throw new ConfigurationException("Can not share same dirs `$a` and `$b`.");
            }
        }
    }

    foreach (get('shared_dirs') as $dir) {
        if (! test("[ -d $sharedPath/$dir ]")) {
            run("mkdir -p $sharedPath/$dir");

            if (test("[ -d $(echo {{release_path}}/$dir) ]")) {
                run("cp -rv {{release_path}}/$dir $sharedPath/" . \dirname(parse($dir)));
            }
        }

        run("rm -rf {{release_path}}/$dir");
        run("mkdir -p `dirname {{release_path}}/$dir`");
        run("{{bin/symlink}} ../../../../shared/$dir {{release_path}}/$dir");
    }
});

/**
 * Override copy dirs task
 */
desc('Copy directories');
task('deploy:copy_dirs', function () {
    $releases = get('releases_list');

    if (isset($releases[0])) {
        foreach (get('copy_dirs') as $dir) {
            $path = "{{deploy_path}}/releases/{$releases[0]}/$dir";

            if (test("[ -d $path ]")) {
                run("mkdir -p {{release_path}}/$dir");
                run("rsync -av $path/ {{release_path}}/$dir");
            }
        }
    }
});

desc('Simple pull the project');
task('deploy:simple_deploy', function () {
    $git = get('bin/git');

    run("cd {{deploy_path}} && $git pull 2>&1");
    run('cd {{deploy_path}} && {{bin/composer}} {{composer_options}} 2>&1');
});

desc('Create robots.txt');
task('nextlevels:robots', function () {
    run('echo "User-agent: *\n Disallow: /" > {{release_path}}/web/robots.txt');
})->onlyForStage('testing');

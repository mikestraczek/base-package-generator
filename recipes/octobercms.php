<?php namespace Deployer;

add('shared_dirs', ['web/storage/app']);
add('copy_dirs', ['web/modules', 'web/plugins/rainlab']);

/**
 * Custom tasks
 */
desc('Create env file');
task('october:env', function () {
    $stage = '{{default_stage}}';

    if (input()->getArgument('stage') !== null) {
        $stage = input()->getArgument('stage');
    }

    run('echo "APP_ENV=' . $stage . '" > {{release_path}}/web/.env');
});

desc('Update migrations');
task('october:migrate', function () {
    run('{{bin/php}} {{release_path}}/web/artisan october:up');
});
